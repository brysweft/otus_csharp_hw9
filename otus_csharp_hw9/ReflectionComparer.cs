﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw9
{
    public class ReflectionComparer : IEqualityComparer<object>
    {
        public new bool Equals(object x, object y)
        {
            return CompareObjectsInternal(x.GetType(), x, y);
        }

        public int GetHashCode(object obj)
        {
            return obj.GetHashCode();
        }

        private bool CompareObjectsInternal(Type type, object x, object y)
        {
            // Если ссылки указывают на один и тот же объект
            if (ReferenceEquals(x, y)) return true;

            // Один из объектов равен null
            if (ReferenceEquals(x, null) != ReferenceEquals(y, null)) return false;

            // Объекты имеют разные типы
            if (x.GetType() != y.GetType()) return false;

            // Строки
            if (Type.GetTypeCode(type) == TypeCode.String) return ((string)x).Equals((string)y);

            // Ссылочные типы
            if (type.IsClass || type.IsInterface) return CompareAllProperties(type, x, y);

            // Примитивные типы или типы перечислений 
            if (type.IsPrimitive || type.IsEnum) return x.Equals(y);

            return x.Equals(y);
        }

        private bool CompareAllProperties(Type type, object x, object y)
        {
            var properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            var readableNonIndexers = properties.Where(p => p.CanRead && p.GetIndexParameters().Length == 0);

            foreach (PropertyInfo propertyInfo in readableNonIndexers)
            {
                var a = propertyInfo.GetValue(x, null);
                var b = propertyInfo.GetValue(y, null);

                if (!CompareObjectsInternal(propertyInfo.PropertyType, a, b)) return false;
            }

            return true;
        }

    }
}