﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw9.Models
{
    public class Warrior : Human, IMyCloneable<Warrior>, ICloneable
    {
        public int Health { get; set; }
        public int MaxHealth { get; set; }
        public int Strength { get; set; }
        public int Durability { get; set; }
        public int Velocity { get; set; }

        public Warrior(string name, int age, int health) : base(name, age)
        {
            Health = health;
            MaxHealth = health;
        }

        public Warrior(Warrior warrior) : base(warrior)
        {
            Health      = warrior.Health;
            MaxHealth   = warrior.MaxHealth;
            Strength    = warrior.Strength;
            Durability  = warrior.Durability;
            Velocity    = warrior.Velocity;
        }
        public override Warrior MyClone()
        {
            Warrior warrior = new Warrior(this);
            return warrior;
        }
    }
}
