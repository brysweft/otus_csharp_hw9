﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw9.Models
{
    public class Shield : Weapon, IMyCloneable<Shield>, ICloneable
    {
        public Shield(string name, string description, int size) : base(name, description, size)
        {
        }
        public Shield(Shield shield) : base(shield)
        {
        }
        public void SetProperties(int armor)
        {
            Armor = armor;
        }

        public override Shield MyClone()
        {
            Shield shield = new Shield(this);
            return shield;
        }
    }
}
