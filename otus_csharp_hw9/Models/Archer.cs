﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace otus_csharp_hw9.Models
{
    public class Archer : Warrior, IMyCloneable<Archer>, ICloneable
    {
        public RangedWeapon Weapon { get; set; }

        public Archer(string name, int age, int health, RangedWeapon weapon) : base(name, age, health)
        {
            Weapon = weapon;
        }

        public Archer(Archer archer) : base(archer)
        {
            Weapon = archer.Weapon.MyClone();
        }

        public override Archer MyClone()
        {
            Archer archer = new Archer(this);
            return archer;
        }

        public override string ToString()
        {
            return $"{nameof(Archer)}: {JsonSerializer.Serialize(this, new JsonSerializerOptions { Converters = { new JsonStringEnumConverter() } })}";
        }
    }
}
