﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw9.Models
{
    public class OneHandedWeapon : Weapon, IMyCloneable<OneHandedWeapon>, ICloneable
    {
        public OneHandedWeapon(string name, string description, int size) : base(name, description, size)
        {
        }
        public OneHandedWeapon(OneHandedWeapon weapon) : base(weapon)
        {
        }
        public override OneHandedWeapon MyClone()
        {
            OneHandedWeapon weapon = new OneHandedWeapon(this);
            return weapon;
        }
    }
}
