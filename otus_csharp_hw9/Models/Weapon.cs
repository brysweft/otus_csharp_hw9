﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw9.Models
{
    public class Weapon : Subject, IMyCloneable<Weapon>, ICloneable
    {
        public int Damage { get; set; }
        public int Armor { get; set; }
        public int Distance { get; set; }

        public Weapon(string name, string description, int size) : base(name, description, size)
        {
        }
        public Weapon(Weapon weapon) : base(weapon)
        {
            Damage = weapon.Damage;
            Armor = weapon.Armor;
            Distance = weapon.Distance;
        }

        public void SetProperties(int damage, int armor, int distance)
        {
            Damage = damage;
            Armor = armor;
            Distance = distance;
        }

        public override Weapon MyClone()
        {
            Weapon weapon = new Weapon(this);
            return weapon;
        }

    }
}
