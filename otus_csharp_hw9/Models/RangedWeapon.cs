﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw9.Models
{
    public class RangedWeapon : Weapon, IMyCloneable<RangedWeapon>, ICloneable
    {
        public RangedWeapon(string name, string description, int size) : base(name, description, size)
        {
        }
        public RangedWeapon(RangedWeapon weapon) : base(weapon)
        {
        }

        public override RangedWeapon MyClone()
        {
            RangedWeapon weapon = new RangedWeapon(this);
            return weapon;
        }
    }
}
