﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw9.Models
{
    public class Human : IMyCloneable<Human>, ICloneable
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public Human(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public Human(Human human)
        {
            Name    = human.Name.Clone().ToString()??"";
            Age     = human.Age;
        }

        public virtual object Clone()
        {
            return MyClone();
        }
        public virtual Human MyClone()
        {
            return new Human(this);
        }
    }
}
