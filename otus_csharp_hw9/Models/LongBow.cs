﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw9.Models
{
    public class LongBow : RangedWeapon, IMyCloneable<LongBow>, ICloneable
    {
        public LongBow(string name, string description, int size) : base(name, description, size)
        {
        }
        public LongBow(LongBow bow) : base(bow)
        {
        }
        public override LongBow MyClone()
        {
            LongBow bow = new LongBow(this);
            return bow;
        }
    }
}
