﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw9.Models
{
    public class Subject : IMyCloneable<Subject>, ICloneable
    {
        public string Name { get; init; }
        public string Description { get; init; }
        public int Size { get; init; }

        public Subject(string name, string description, int size)
        {
            Name = name;
            Description = description;
            Size = size;
        }

        public Subject(Subject subject)
        {
            Name        = subject.Name.Clone().ToString() ?? "";
            Description = subject.Description.Clone().ToString() ?? "";
            Size        = subject.Size;
        }
        public virtual object Clone()
        {
            return MyClone();
        }
        public virtual Subject MyClone()
        {
            return new Subject(this);
        }
    }
}
