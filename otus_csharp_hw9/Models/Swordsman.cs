﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace otus_csharp_hw9.Models
{
    public class Swordsman : Warrior, IMyCloneable<Swordsman>, ICloneable
    {
        public OneHandedWeapon WeaponRightHand { get; set; }
        public Shield WeaponLeftHand { get; set; }

        private bool IsTortoise { get; set; }

        public Swordsman(string name, int age, int health, OneHandedWeapon weaponRightHand, Shield weaponLeftHand) : base(name, age, health)
        {
            WeaponRightHand = weaponRightHand;
            WeaponLeftHand = weaponLeftHand;
        }

        public Swordsman(Swordsman swordsman) : base(swordsman)
        {
            WeaponRightHand = swordsman.WeaponRightHand.MyClone();
            WeaponLeftHand  = swordsman.WeaponLeftHand.MyClone();
            IsTortoise      = swordsman.IsTortoise;
        }

        public void UpShield(bool shieldIsUp)
        {
            IsTortoise = shieldIsUp;
        }

        public override Swordsman MyClone()
        {
            Swordsman swordsman = new Swordsman(this);
            return swordsman;
        }

        public override string ToString()
        {
            return $"{nameof(Swordsman)}: {JsonSerializer.Serialize(this, new JsonSerializerOptions { Converters = { new JsonStringEnumConverter() } })}";
        }


    }
}
