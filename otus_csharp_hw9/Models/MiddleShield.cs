﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw9.Models
{
    public class MiddleShield : Shield, IMyCloneable<MiddleShield>, ICloneable
    {
        public MiddleShield(string name, string description, int size) : base(name, description, size)
        {
        }
        public MiddleShield(MiddleShield shield) : base(shield)
        {
        }
        public override MiddleShield MyClone()
        {
            MiddleShield shield = new MiddleShield(this);
            return shield;
        }
    }
}
