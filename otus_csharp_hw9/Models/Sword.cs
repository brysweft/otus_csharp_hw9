﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw9.Models
{
    public class Sword : OneHandedWeapon, IMyCloneable<Sword>, ICloneable
    {
        public Sword(string name, string description, int size) : base(name, description, size)
        {
        }
        public Sword(Sword sword) : base(sword)
        {
        }
        public override Sword MyClone()
        {
            Sword sword = new Sword(this);
            return sword;
        }
    }
}
