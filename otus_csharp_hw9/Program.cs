﻿using otus_csharp_hw9.Models;

namespace otus_csharp_hw9
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" *** Паттерн Прототип *** ");

            RunTest_IPrototype();
            RunTest_IClonable();

        }

        static void RunTest_IPrototype() {

            Console.WriteLine();
            Console.WriteLine("Тест реализации IMyCloneable");

            Swordsman swordsman = GetTestData_Swordsman();
            Archer archer = GetTestData_Archer();

            Console.WriteLine();
            Console.WriteLine("Клонирование мечника...");
            var swordsmanClone = swordsman.MyClone();
            CheckObjectEquality(swordsman, swordsmanClone);
            Console.WriteLine("Обозвали клона мечника:");
            swordsmanClone.Name = "Pashka Zloy";
            CheckObjectEquality(swordsman, swordsmanClone);

            Console.WriteLine();
            Console.WriteLine("Клонирование еще одногомечника...");
            var swordsmanClone2 = swordsman.MyClone();
            Console.WriteLine("Вручили клону такой же меч...");
            swordsmanClone2.WeaponRightHand = swordsman.WeaponRightHand.MyClone();
            CheckObjectEquality(swordsman, swordsmanClone2);
            Console.WriteLine("Заточили меч клона на +2 ед.:");
            swordsmanClone2.WeaponRightHand.Damage += 2;
            CheckObjectEquality(swordsman, swordsmanClone2);

            Console.WriteLine();
            Console.WriteLine("Клонирование лучника...");
            var archerClone = archer.MyClone();
            CheckObjectEquality(archer, archerClone);
            Console.WriteLine("Обозвали клона лучника:");
            archerClone.Name = "Petryshka Skromniy";
            CheckObjectEquality(archer, archerClone);

            Console.WriteLine();
            Console.WriteLine("Клонирование еще одного лучника...");
            var archerClone2 = archer.MyClone();
            CheckObjectEquality(archer, archerClone2);
            Console.WriteLine("Напоили лучника самогоном: +5 к смелости, -2 к скорости:");
            archerClone2.Durability += 5;
            archerClone2.Velocity -= 2;
            CheckObjectEquality(archer, archerClone2);

        }
        static void RunTest_IClonable()
        {
            Console.WriteLine();
            Console.WriteLine("Тест реализации IClonable");

            Swordsman swordsman = GetTestData_Swordsman();
            Archer archer = GetTestData_Archer();

            Console.WriteLine();
            Console.WriteLine("Клонирование мечника...");
            var swordsmanClone = swordsman.Clone();
            CheckObjectEquality(swordsman, swordsmanClone);

            Console.WriteLine();
            Console.WriteLine("Клонирование еще одногомечника...");
            var swordsmanClone2 = swordsman.Clone();
            Console.WriteLine("Сравнили двух клонов...");
            CheckObjectEquality(swordsmanClone, swordsmanClone2);

            Console.WriteLine();
            Console.WriteLine("Клонирование лучника...");
            var archerClone = archer.Clone();
            CheckObjectEquality(archer, archerClone);

            Console.WriteLine();
            Console.WriteLine("Клонирование еще одного лучника...");
            var archerClone2 = archer.Clone();
            CheckObjectEquality(archer, archerClone2);
            Console.WriteLine("Сравнили двух клонов...");
            CheckObjectEquality(archerClone, archerClone2);
        }
        static void CheckObjectEquality(object a, object b)
        {
            ReflectionComparer reflectionComparer = new ReflectionComparer();

            Console.WriteLine("Исходный объект:");
            Console.WriteLine(a.ToString());
  
            Console.WriteLine("Копия объекта:");
            Console.WriteLine(b.ToString());
            Console.WriteLine($"Объекты эквивалентны: {reflectionComparer.Equals(a, b)}");
        }
        static Swordsman GetTestData_Swordsman()
        {
            Sword sword = new Sword("Rusty sword", "", 4) { Damage = 10 };
            MiddleShield shield = new MiddleShield("Wooden shield", "", 6) { Armor = 2 };
            Swordsman swordsman = new Swordsman("Pashka Dobriy", 25, 120, sword, shield)
            {
                Strength = 5,
                Durability = 4,
                Velocity = 4,
            };
            return swordsman;
        }
        static Archer GetTestData_Archer()
        {
            LongBow bow = new LongBow("Regular wooden longbow", "", 6) { Damage = 6, Distance = 20 };
            Archer archer = new Archer("Petryshka Boltun", 21, 100, bow)
            {
                Strength = 3,
                Durability = 2,
                Velocity = 6,
            };
            return archer;
        }

    }
}