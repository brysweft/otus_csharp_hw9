﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw9
{
    public interface IMyCloneable<out T>
    {
        public T MyClone();
    }
}
